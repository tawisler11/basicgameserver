#include <QString>
#include <QPair>
#include <stdint.h>

class Player
{
public:
	struct Color
	{
		Color():red(0),green(0),blue(0){};
		uint8_t red;
		uint8_t green;
		uint8_t blue;
	};

private:

    QString username;
    QPair<int,int> location;
    bool up;
    bool down;
    bool left;
    bool right;
    Color color;

    static const unsigned int MOVESPEED = 3;

public:

	Player();
	~Player();

	void move();

    inline QString getUsername(){return username;}
    inline QPair<int,int> getLocation(){return location;}
    inline Color getColor(){return color;}

    inline void setUsername(QString username){this->username = username;}
    inline void setLocation(QPair<int,int> location){this->location = location;}
    inline void setColor(Color color){this->color = color;}
    inline void setUp(bool up){this->up = up;}
    inline void setDown(bool down){this->down = down;}
    inline void setLeft(bool left){this->left = left;}
    inline void setRight(bool right){this->right = right;}
};
