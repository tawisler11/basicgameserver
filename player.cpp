#include "player.h"

//Default constructor
Player::Player() : 
username(""), location(QPair<int,int>(0,0)), up(false), down(false), left(false), right(false), color(Color())
{
}

//Destructor
Player::~Player()
{
}

//Movement
void Player::move()
{
	if(up == true && location.second >= 1)
    {
        location.second -= MOVESPEED;
    }
    if(down == true && location.second <= 510)
    {
        location.second += MOVESPEED;
    }
    if(left == true && location.first >= 1)
    {
        location.first -= MOVESPEED;
    }
    if(right == true && location.first <= 900)
    {
        location.first += MOVESPEED;
    }
}