#include <QCoreApplication>
#include "basicgameserver.h"
#include "iostream"

#include <QtSql>

// testing git in st3
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    basicGameServer *server = new basicGameServer();
    bool success = server->listen(QHostAddress::Any, 4200);
    if(!success)
    {
        qFatal("Could not listen on port 4200.");
    }

    qDebug() << "Ready";

    /*
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("localhost");
    db.setDatabaseName("W:/Software/basicGame/basicgameserver/thedb.db");
    db.setPassword("");
    db.setUserName("");

    if(!db.open())
    {
    	qDebug() << "database failed to open";
    }
    else
    {
    	qDebug() << "database opened";
    }

    if(db.isDriverAvailable("QSQLITE"))
    {
    	qDebug() << "driver is available";
    }

    QString theInput;
    char input[256];
    std::cin >> input;
    theInput = QString::fromUtf8(input);
    qDebug() << "input:" << theInput;

    QSqlQuery records = db.exec("SELECT Password FROM Authentication_T WHERE Username = '" + theInput + "'");
    QSqlError errors = db.lastError();
    QString theError = errors.databaseText();

    if(!theError.isEmpty())
    {
    	qDebug() << theError;
    }

    while(records.next())
    {
    	qDebug() << "got a value";
    	QString name = records.value(0).toString();
    	qDebug() << name;
    }
    */

    return a.exec();
}
