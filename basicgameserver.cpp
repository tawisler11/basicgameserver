#include "basicgameserver.h"

#include <QTcpSocket>
#include <QRegExp>
#include <time.h>

basicGameServer::basicGameServer(QObject *parent) : QTcpServer(parent)
{
    srand(time(NULL));

    timer = new QTimer();
    timer->start(1000/80);
    connect(timer, SIGNAL(timeout()), this, SLOT(fps()));

    timer1 = new QTimer();
    timer1->start(1000/80);
    connect(timer1, SIGNAL(timeout()), this, SLOT(updateClients()));
}

void basicGameServer::fps()
{
    foreach(QTcpSocket *client, clients)
    {
        players[client]->move();

        // foreach(QTcpSocket *otherClient, clients)
        //         otherClient->write(QString(players[client]->getUsername() +
        //             ":x" + QString::number(players[client]->getLocation().first) +
        //             ",y" + QString::number(players[client]->getLocation().second) + "\n").toUtf8());
    }
    
}

void basicGameServer::updateClients()
{
    foreach(QTcpSocket *client, clients)
    {
        foreach(QTcpSocket *otherClient, clients)
                otherClient->write(QString(players[client]->getUsername() +
                    ":x" + QString::number(players[client]->getLocation().first) +
                    ",y" + QString::number(players[client]->getLocation().second) + "\n").toUtf8());
    }
    
}

void basicGameServer::incomingConnection(int socketfd)
{
    QTcpSocket *client = new QTcpSocket(this);

    if(1/*game isn't full*/)//TODO
    {
        //Generate list of players to send to new player, after connected
        QString playerList = QString("");

        foreach(QTcpSocket *otherClient, clients)
        {
            playerList = QString(playerList + players[otherClient]->getUsername() +
                            "," + QString::number(players[otherClient]->getLocation().first) + 
                            "," + QString::number(players[otherClient]->getLocation().second) + 
                            "," + QString::number(players[otherClient]->getColor().red) + 
                            "," + QString::number(players[otherClient]->getColor().green) +
                            "," + QString::number(players[otherClient]->getColor().blue) +
                            ":"
                         );
        }

        qDebug() << playerList;
        
        //Sign up the new player
        client->setSocketDescriptor(socketfd);
        clients.insert(client);
        
        players[client] = new Player();
        Player::Color color;
        color.red = rand() % 256;
        color.green = rand() % 256;
        color.blue = rand() % 256;
        players[client]->setColor(color);

        qDebug() << "New client from:" << client->peerAddress().toString();

        connect(client, SIGNAL(readyRead()), this, SLOT(readyRead()));
        connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));

        //Finally send the list of players
        if(!playerList.isEmpty())
            client->write(QString("/players:" + playerList + "\n").toUtf8());
    }
}

void basicGameServer::readyRead()
{
    QTcpSocket *client = (QTcpSocket*)sender();
    while(client->canReadLine())
    {
        QString line = QString::fromUtf8(client->readLine()).trimmed();
        qDebug() << "Read line:" << line;

        QRegExp meRegex("^/me:(.*):r(.*):g(.*):b(.*)$");
        QRegExp availableRegex("^/available:(.*)$");
        QRegExp moveRegex("^/move:(.*)$");

        if((meRegex.indexIn(line) != -1) && (players[client] != NULL))
        {
            QString user = meRegex.cap(1);

            bool exists = false;
            foreach(QTcpSocket *otherClient, clients)
            {
                if(players[otherClient]->getUsername() == user)
                {
                    exists = true;
                    break;
                }
            }

            if(!exists)
            {
                client->write(QString("/accepted:true\n").toUtf8());
                players[client]->setUsername(user);

                Player::Color color;
                color.red = meRegex.cap(2).toInt();
                color.green = meRegex.cap(3).toInt();
                color.blue = meRegex.cap(4).toInt();
                players[client]->setColor(color);

                foreach(QTcpSocket *theClient, clients)
                {
                    theClient->write(QString("Server:" + user + " has joined.\n").toUtf8());

                    QString thePlayer = QString(players[client]->getUsername() +
                                "," + QString::number(players[client]->getLocation().first) + 
                                "," + QString::number(players[client]->getLocation().second) + 
                                "," + QString::number(color.red) + 
                                "," + QString::number(color.green) +
                                "," + QString::number(color.blue)
                                );
                    theClient->write(QString("/player:" + thePlayer + "\n").toUtf8());
                }
                sendUserList();
            }
            else
            {
                client->write(QString("/accepted:false\n").toUtf8());
            }
        }
        else if((availableRegex.indexIn(line) != -1) && (players[client] != NULL))
        {
            QString name = availableRegex.cap(1);
            qDebug() << "availability:" << name;
            bool exists = false;
            foreach(QTcpSocket *otherClient, clients)
            {
                if(players[otherClient]->getUsername() == name)
                {
                    exists = true;
                    break;
                }
            }
            QString availability = exists? QString("/availablility:false\n") : QString("/availablility:true\n");

            qDebug() << "availabilityMsg:" << availability;
            client->write(availability.toUtf8());
        }
        else if((moveRegex.indexIn(line) != -1) && (players[client] != NULL))
        {
            qDebug() << "REACHED FUNCTION ----";
            if(line == "/move:up true")
            {
                qDebug() << "up";
                players[client]->setUp(true);
            }
            else if(line == "/move:down true")
            {
                qDebug() << "down";
                players[client]->setDown(true);
            }
            else if(line == "/move:left true")
            {
                qDebug() << "left";
                players[client]->setLeft(true);
            }
            else if(line == "/move:right true")
            {
                qDebug() << "right";
                players[client]->setRight(true);
            }
            else if(line == "/move:up false")
            {
                qDebug() << "up";
                players[client]->setUp(false);
            }
            else if(line == "/move:down false")
            {
                qDebug() << "down";
                players[client]->setDown(false);
            }
            else if(line == "/move:left false")
            {
                qDebug() << "left";
                players[client]->setLeft(false);
            }
            else if(line == "/move:right false")
            {
                qDebug() << "right";
                players[client]->setRight(false);
            }
        }
        else if(players[client] != NULL)
        {
            QString message = line;
            QString user = players[client]->getUsername();

            qDebug() << "User:" << user;
            qDebug() << "Message:" << message;

            foreach(QTcpSocket *otherClient, clients)
                otherClient->write(QString(user + ":" + message + "\n").toUtf8());
        }
        else
        {
            qWarning() << "Got bad message from client:" << client->peerAddress().toString() << line;
        }
    }
}

void basicGameServer::disconnected()
{
    QTcpSocket *client = (QTcpSocket*)sender();
    qDebug() << "Client disconnected:" << client->peerAddress().toString();

    QString user = players[client]->getUsername();
    delete players[client];
    players[client] = NULL;
    clients.remove(client);

    sendUserList();
    foreach(QTcpSocket *client, clients)
    {
        client->write(QString("Server:" + user + " has left.\n").toUtf8());
        client->write(QString("/disconnected:" + user + "\n").toUtf8());
    }
}

void basicGameServer::sendUserList()
{
    QStringList userList;

    //TODO: this is super stupid. pls combine for loops
    foreach(QTcpSocket* client, clients)
        userList << players[client]->getUsername();

    foreach(QTcpSocket *client, clients)
        client->write(QString("/users:" + userList.join(",") + "\n").toUtf8());
}
