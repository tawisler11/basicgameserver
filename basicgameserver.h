#ifndef BASICGAMESERVER_H
#define BASICGAMESERVER_H

#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>
#include <QSet>
#include <QPair>
#include <QTimer>

#include "player.h"

class basicGameServer : public QTcpServer
{
    Q_OBJECT

    struct Directions
    {
        bool up;
        bool down;
        bool left;
        bool right;
    };

    public:
        basicGameServer(QObject *parent=0);

    private slots:
        void readyRead();
        void disconnected();
        void sendUserList();
        void fps();
        void updateClients();

    protected:
        void incomingConnection(int socketfd);

    private:
        QMap<QTcpSocket*, Player*> players; 
        QSet<QTcpSocket*> clients;
        QTimer *timer;
        QTimer *timer1;
};

#endif
