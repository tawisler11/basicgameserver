#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T00:49:28
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

TARGET = basicGameServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    basicgameserver.cpp \
    player.cpp

HEADERS += \
    basicgameserver.h \
    player.h
