@echo off
mkdir ..\build\
mkdir ..\..\deliverables\

pushd ..\build\
qmake ..\src\basicGameServer.pro
mingw32-make -f MakeFile.Debug clean
mingw32-make -f MakeFile.Debug
pushd debug
xcopy basicGameServer.exe ..\..\..\deliverables\ /y
popd
popd
